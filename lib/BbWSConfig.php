<?php

/*****************************************************
* PHP Interface to Blackboard Web Service: Config Class                             
* Developed by: Brandon Peters	                                       
* Updated: 11/10/2014	                                                       
*****************************************************/

class BbWSConfig {

	//Enter domain URL of your Blackboard instance
	protected $appserver = "[Enter Blackboard server domain here | ex. example.school.edu]";
	
	//Enter your proxy tool configurations
	protected $password = "[Enter the shared secret used to register the proxy tool]";
	protected $clientVendorId = "[Enter the VenderId used to register the proxy tool]";
	protected $clientProgramId = "[Enter the ProgramId used to register the proxy tool]";

}

?>