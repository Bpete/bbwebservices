<?php

/*****************************************************
* PHP Interface to Blackboard Web Service                             
* Developed by: Brandon Peters	                                       
* Updated: 11/10/2014	                                                       
*****************************************************/


class BbWSInterface extends BbWSConfig{

	private $session_pass = Null;

	function initialize() {
	
		//------------------------------------------------------//
		// Initialize a Session
		//------------------------------------------------------//
		
		$url = "https://".$this->appserver."/webapps/ws/services/Context.WS?wsdl";

		$option=array('location'=>$url);
			
		$client = new WSSoapClient($url,$option);	
			
		$client->__setUsernameToken('session','nosession');
		
		$result=$client->__soapCall('initialize',array());
		
		$this->session_pass = $result->return;

		//------------------------------------------------------//
		//  Login to Proxy Tool
		//------------------------------------------------------//

		$client->__setUsernameToken('session',$this->session_pass); 
		$params = array("loginTool"=>array(
		
			"password"=>$this->password,
			"clientVendorId"=>$this->clientVendorId,
			"clientProgramId"=>$this->clientProgramId
	 
		));

		$client->__soapCall('loginTool',$params);
		
		return $this->session_pass;
		
	} // End Initialize Function
		
	function bbcall($service,$method,$params) {
	
		//------------------------------------------------------//
		// Bb Service Calls
		//------------------------------------------------------//
	 
		$this->initialize();
	 
		$url = "https://".$this->appserver."/webapps/ws/services/".$service.".WS?wsdl";
	  
		$option=array('location'=>$url);
	 
		$bbcall = new WSSoapClient($url,$option);

		$bbcall->__setUsernameToken('session',$this->session_pass);
	  
		$response=$bbcall->__soapCall($method,$params);
		
		//Convert response to array for easier handling	
		$json  = json_encode($response);
		$array = json_decode($json, true);
		
		return $array;
		
	 } //End bbcall Function
 
	function registerProxyTool($proxy) {
	
		//------------------------------------------------------//
		// Register Proxy Tool
		//------------------------------------------------------//

		$url = "https://".$this->appserver."/webapps/ws/services/Context.WS?wsdl";

		$option=array('location'=>$url);
			
		$client = new WSSoapClient($url,$option);	

		$client->__setUsernameToken('session','nosession'); 
		
		return $client->__soapCall('registerTool',$proxy);
		
	}
 
}
 
?>