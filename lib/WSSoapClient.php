<?PHP

/*********************************************************************************
 * Copyright (c) 2007, Roger Veciana
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/
 
/*********************************************************************************
 * This class can add WSSecurity authentication support to SOAP clients
 * implemented with the PHP 5 SOAP extension.
 *
 * It extends the PHP 5 SOAP client support to add the necessary XML tags to
 * the SOAP client requests in order to authenticate on behalf of a given
 * user with a given password.
 *
 ********************************************************************************/
 
/*********************************************************************************
* Class: WSSoapClient
* Author: Roger Veciana i Rovira 
* Date: 2006-07-12
* Author Source: http://www.phpclasses.org/browse/author/233806.html
* Modified by: Brandon S Peters                      
* Repository: https://bitbucket.org/Bpete/bbwebservices
 ********************************************************************************/


class WSSoapClient extends SoapClient{

	private $username;
	private $password;


	/*Generates de WSSecurity header*/
	private function wssecurity_header(){

		$timestamp=gmdate('Y-m-d\TH:i:s\Z');//The timestamp. The computer must be on time or the server you are connecting may reject the password digest for security.
		
		$nonce=mt_rand(); //A random word. The use of rand() may repeat the word if the server is very loaded.
		
		$passdigest=base64_encode(pack('H*',sha1(pack('H*',$nonce).pack('a*',$timestamp).pack('a*',$this->password))));//This is the right way to create the password digest. Using the password directly may work also, but it's not secure to transmit it without encryption. And anyway, at least with axis+wss4j, the nonce and timestamp are mandatory anyway.

		$auth='
		<wsse:Security SOAP-ENV:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
			
			<wsu:Timestamp wsu:Id="Timestamp-10" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
				<wsu:Created>'.$timestamp.'</wsu:Created>
			</wsu:Timestamp>

			<wsse:UsernameToken wsu:Id="UsernameToken-9" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
				<wsse:Username>'.$this->username.'</wsse:Username>
				<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">'.$this->password.'</wsse:Password>
				<wsu:Created>'.$timestamp.'</wsu:Created>
			</wsse:UsernameToken>

		</wsse:Security>
		
		';


		$authvalues=new SoapVar($auth,XSD_ANYXML); //XSD_ANYXML (or 147) is the code to add xml directly into a SoapVar. Using other codes such as SOAP_ENC, it's really difficult to set the correct namespace for the variables, so the axis server rejects the xml.
		
		$header=new SoapHeader("http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd", "Security",$authvalues,true);

		return $header;
	}

	//It's necessary to call it if you want to set a different user and password
	public function __setUsernameToken($username,$password){
	
		$this->username=$username;
		$this->password=$password;
		
	}


	/*Overwrites the original method adding the security header. As you can see, if you want to add more headers, the method needs to be modifyed*/
	public function __soapCall($function_name,$arguments,$options=null,$input_headers=null,$output_headers=null){

		$result = parent::__soapCall($function_name,$arguments,$options,$this->wssecurity_header());

		return $result;
		
	}

}//End Class

?>