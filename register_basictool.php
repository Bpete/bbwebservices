<?php

/*****************************************************
* Example Basic Proxy Tool Registration                             
* Developed by: Brandon Peters	                                       
* Updated: 11/10/2014	                                                       
*****************************************************/

function autoLoadClasses($className) {
    
	$path = 'lib/';
	include $path.$className.'.php';

}

spl_autoload_register('autoLoadClasses');

$instance = new BbWSInterface;

$proxy = array("registerTool"=>

	array(
	
	"clientVendorId"=>"[Enter the name of your institution/center/department here]", 
	"clientProgramId"=>"[Enter the name of the app that will use this proxy tool]", 
	"registrationPassword"=>"[Enter the proxy tool registration password you established in the Blackboard Admin GUI under Manage Global Properties]", 
	"description"=>"[Enter a description of your app]", 
	"initialSharedSecret"=>"[Enter a password that will be used by your app]",  
	"requiredToolMethods" => 
		
		array(

			"Context.WS:initialize",
			"Context.WS:loginTool", 
			"Context.WS:getMemberships", 
			"Course.WS:getCourse",
			"CourseMembership.WS:getCourseMembership",
			"User.WS:getUser"
				
		)

	)
);
	
$results = $instance->registerProxyTool($proxy);

echo "<pre>";
print_r($results);
echo "</pre>";

?>