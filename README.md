# PHP Interface to Blackboard Web Services #

This PHP web application is a basic example of how to connect to Blackboard's Web Services API. You will need to have System Administrator (GUI) access to your Blackboard instance (or know someone who does) in order to register the proxy tool. Here is a good tutorial on how to setup proxy tools in Blackboard - just be sure to stop at the **Setup Rampart Module** section since we will be using PHP for registration: [Blackboard Proxy Tool Setup](http://www.brucephillips.name/blog/index.cfm/2010/8/3/Using-Blackboard-Learn-9-Web-Services--Part-1-Logging-In-Using-A-Proxy-Tool). Here is another resource that may come in handy: [Manage Proxy Tools](http://help.blackboard.com/en-us/Learn/9.1_SP_14/Administrator/140_System_Integration/030_Integration_Development/020_Proxy_Tools/Manage_Proxy_Tools)


### Setup ###

In order to use this web app, you will first need to register the proxy tool within your Blackboard instance. Start by editing the array parameters in the following section of the *register_basictool.php* file:

    "clientVendorId"=>"[Enter the name of your institution/center/department here]", 
    "clientProgramId"=>"[Enter the name of the app that will use this proxy tool]", 
    "registrationPassword"=>"[Enter the proxy tool registration password you established in the Blackboard Admin GUI under Manage Global Properties]", 
    "description"=>"[Enter a description of your app]", 
    "initialSharedSecret"=>"[Enter a password that will be used by your app]", 

Next, you will need to edit the following section of the *BbWSConfig.php* class file (located in the /lib directory):

    //Enter domain URL of your Blackboard instance
    protected $appserver = "[Enter Blackboard server domain here | ex. example.school.edu]";

    //Enter your proxy tool configurations
    protected $password = "[Enter the shared secret used to register the proxy tool]";
    protected $clientVendorId = "[Enter the VenderId used to register the proxy tool]";
    protected $clientProgramId = "[Enter the ProgramId used to register the proxy tool]";    

Next, run the *register_basictool.php* script which will register the proxy tool with your Blackboard instance. If successful, you will see this new proxy tool under the Proxy Tools listing within the Blackboard System Admin GUI (Building Blocks -> Proxy Tools). You will then need to make the proxy tool available (via the GUI) before calls to Bb web services can be made.  

### Using the App ###

Now that you have setup and registered your proxy tool, we can now start making calls to Bb web services. There are several method calls in the *index.php* script to get your started. For example, lets use **getUser** which will allow us to view a list of attributes for a particular Blackboard user account. Locate the following section of the *index.php* script, uncomment the *$username*, *$service*, *$method*, and *$params* variables, and then enter a Bb username:

    //-----------------------
    // getUser Method Example: Parameter is the user's Blackboard username | Returns array containing the selected user's information
    //-----------------------
    $username = "[Enter Bb Username here]";		
    $service = "User";
    $method = "getUser";
    $params = array($method=>array("filter"=>array("filterType"=>6, "name"=>$username)));

Next, run the *index.php* script. If successful, you will see a return array containing that Bb user's attributes. 

```
#!php

Array
(
    [return] => Array
        (
            [birthDate] => 0
            [dataSourceId] => _2_1
            [educationLevel] => blackboard.data.user.User$EducationLevel:UNKNOWN
            [expansionData] => USER.UUID
            [extendedInfo] => Array
                (
                    [businessFax] => 
                    [businessPhone1] => 270-555-5555
                    [businessPhone2] => 
                    [city] => Faketown
                    [company] => ABC University
                    [country] => USA
                    [department] => Instructional Technology
                    [emailAddress] => email@example.edu
                    [expansionData] => Array
                        (
                            [0] => USER.OTHERNAME=
                            [1] => USER.SUFFIX=
                        )

                    [familyName] => Doe
                    [givenName] => John
                    [homeFax] => 
                    [homePhone1] => 
                    [homePhone2] => 
                    [jobTitle] => 
                    [middleName] => 
                    [mobilePhone] => 
                    [state] => Kentucky
                    [street1] => 
                    [street2] => 
                    [webPage] => 
                    [zipCode] => 
                )

            [genderType] => blackboard.data.user.User$Gender:UNKNOWN
            [id] => _XXXXXX_1
            [insRoles] => Array
                (
                    [0] => role_1
                    [1] => role_2
                    [2] => role_3
                    [3] => role_4
                    [4] => role_5
                    [5] => role_6
                )

            [isAvailable] => 1
            [name] => userdoe
            [password] => 
            [studentId] => XXXXXXXXX
            [systemRoles] => USER
            [title] => 
            [userBatchUid] => userdoe
        )

)

```
### Resources ###

[Blackboard Web Services API Documentation](http://help.blackboard.com/en-us/Learn/9.1_SP_14/Administrator/230_Developer_Resources/000_Building_Blocks_APIs)

[Administering Bb Web Services](http://help.blackboard.com/en-us/Learn/9.1_SP_14/Administrator/140_System_Integration/030_Integration_Development/030_Web_Services)