<?php

/*****************************************************
* Example Method Calls to Blackboard Web Service                             
* Developed by: Brandon Peters	                                       
* Updated: 11/10/2014	                                                       
*****************************************************/

function autoLoadClasses($className) {
    
	$path = 'lib/';
	include $path.$className.'.php';

}

spl_autoload_register('autoLoadClasses');

$instance = new BbWSInterface;

//-----------------------
// getUser Method Example: Parameter is the user's Blackboard username | Returns array containing the selected user's information
//-----------------------
//$username = "[Enter Bb Username here]";		
//$service = "User";
//$method = "getUser";
//$params = array($method=>array("filter"=>array("filterType"=>6, "name"=>$username)));		

//-----------------------
// getCourse Method Example: Parameter is the course 'id' in the following format - _XXXX_1 | Returns array containing the selected course's information
//-----------------------
//$course_id = "[Enter course id as _XXXX_1]";
//$service = "Course";
//$method = "getCourse";
//$params = array($method=>array("filter"=>array("filterType"=> 3,"ids"=>$course_id)));	

//-----------------------
// getCourseMembership Method Example: Parameter is the course 'id' in the following format - _XXXX_1 | Returns array of course enrollments including course role, user id, enrollment id, etc.
//-----------------------	
//$course_id = "[Enter course id as _XXXX_1]";
//$service = "CourseMembership";
//$method = "getCourseMembership";
//$params = array($method=>array("courseId" => $course_id, "f"=>array("filterType"=>2, "courseIds"=>$course_id)));

//-----------------------
// getMemberships Method Example: Parameter is the user's Blackboard username | Returns array of course ids in which the user is enrolled
//-----------------------
//$username = "[Enter Bb Username here]";
//$service = "Context";
//$method = "getMemberships";
//$params = array($method=>array("userid"=>$username));


$results = $instance->bbcall($service,$method,$params);


echo "<h1 style='color:red'>".$method." Response</h1>";
echo "<pre>";
print_r($results);
echo "</pre>";

?>